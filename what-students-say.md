# What Students Say

(admittedly, I cherry-picked the praise but do promise to take the criticisms on board as well)

A small section of the course that I really found useful was spending the time to learn about debuggers. In my entire career, I have never been taught how to use a debugger nor how helpful they could be. Having the one assignment, BlocklyPrisonnerDilemma, that focused on working code through with a debugger was extremely useful. I now feel I have a better understanding of what the code is doing step by step and applying topics I have learned from this class and others regarding things such as variable typing.

The prospect of creating a language, even if rudimentary in nature, was an eye-opening experience Prior to this course, the idea of crafting a pro-gramming language had never crossed my mind.

CPSC 354: Programming Languages is not just a course; it’s a thrilling adventure into the very essence of these languages. It goes beyond the surface of coding, guiding us through a profound exploration of the intricate designs, structures, and theoretical foundations that shape programming languages. It’s an opportunity to unravel the mysteries of the languages computers comprehend.

The most useful aspect of the course, in my opinion, was the project on develping a Domain Specific Language (DSL) for home automation. I found this project important because it bridged the gap between theoretical concepts and practical utility. Working on this open-source project, I experienced firsthand the challenges of software development – from conceptualization to debugging and user interface design. This project was not only an exercise in applying classroom knowledge but also a lesson in the dynamics of open-source contributions, which are pivotal in today’s collaborative software development environment.

this course improved my logical and critical thinking, which are skills indispensable in software engineering. Every class always presented an opportnity to engage and stimulate your mind. We were challeneged and encourage to think differnetly to navigate a multitude of situations. For example, the Prisoners’ Hats problem was a beautiful example of looking at a real world issue like a software engineer.

In the wider software engineering landscape, the focal theories and concepts echo the industry’s shift towards functional programming, with languages like Scala and Kotlin emphasizing immutability and higher- order functions, reducing side-effects that are often a source of bugs in software. This aligns with the industry’s continuous pursuit of reliable and scalable systems, especially in distributed and concurrent computing contexts.

Most interesting was Haskell’s pure functional approach that starkly contrasts with imperative languages, compelling a rethinking of problem-solving strategies and promoting a deeper understanding of computation and side-effect management—an invaluable mindset for developing complex systems.

Debugging was a whole ordeal, especially with our inexperience in the languages we used. However, I soon found out that debugging had less to do with knowing the languages inside and out and more to do with genuine problem solving as well as a basic understanding of what your code is doing. This was a breakthrough for me, and perhaps one of the most important skills I picked up on.

One aspect of this project process that I really enjoyed was the pull requests and issues provided from other students within this class. I felt like this was a good simulation of how software engineers manage and improve their projects in the real world and in industry.

In the other computer science classes I have taken throughout my academic calendar, I have never learned how to properly use github pull requests and report issues.

Understanding these theoretical underpinnings not only enhances problem-solving abilities but also cultivates a mindset focused on designing robust, scalable, and efficient software systems. It also gave me a much deeper understanding of a lot of the reasons we do things in certain languages.

The practical applications derived from theoretical concepts were very interesting. For instance, parsing grammars laid the groundwork for compilers and language interpreters. Additionally, the exploration of algorithms for type inference illuminated the process behind identifying variable types, a critical aspect of modern language design. These insights bridged the gap between theory and practice, illustrating how foundational theories manifest in real-world software development.

CPSC-354 provided a robust foundation in theoretical computer science concepts vital for aspiring software engineers.

I feel like many courses focus too heavily on the practical implementations, where this course also had a healthy amount of theory.

One thing that I learned was the use of GitHub as a collaborative tool.

I liked how we had peer reviews of other groups projects and how we got to have other members of other groups look at our project and add useful suggestions.

One big thing I’ve learned is the power of recursion.




