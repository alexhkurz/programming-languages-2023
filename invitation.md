# Invitation

You already know some programming languages (such as Python, Java, or C++) and you are able to teach yourself a new one. But did you ever wonder how to design your own programming language? How would you even get started on a project like this? 

We will learn how to define the syntax of a programming language using a context-free grammar, how to use a parser generator and how to write an interpreter by recursion on abstract syntax.

In a group project you will put these skills to practice and implement your own small programming language.

- Our first programming language will just be high-school arithmetic and the interpreter will be a simple calculator. To appreciate that even this is not such an easy task, I invite you to implement from scratch a calculator in your favourite programming language. 
- Our second example, will be the smallest proper programming language, lambda calculus. For this we will need to learn about variable binding and recursion, two techniques at the heart of all programming languages. 
- If time permits, we extend lambda calculus to a mixed-paradigm language including higher-order functions and recursion from lambda-calculus and adding assignments, loops, pointers, and arrays.

On the way, we will introduce various theoretical concepts that underpin Programming Languages and open windows to topics in programming languages research such as

- theory of term rewriting
- type theory
- program verification

We will also have the opportunity to touch on foundational questions such as "What is Computation?", "What is Meaning?", "What is Information?", "What is an Algorithm?" and discuss whether it is possible to give precise answers.

## Some thoughts on why it is worth learning theory

In this course, mathematics (in particular discrete mathematics) will play an important role. Here are some reasons why I think that it is worth spending time on understanding the foundations of Programming Languages:

- Theory stands the test of time. [^goodtheory]

- Theory gives rise to algorithms. [^algorithms]

- Theory gives rise to problem solving techniques that change the way you are going to look at problems. [^problemsolving] 

[^goodtheory]: Turing machines, Church's lambda calculus, Shannon's information theory, Kolmogorov probability, Knuth's parsing, Cook's NP-completeness, Hoare's logic, Scott's domain theory , Lamport's byzantine fault tolerance, Milner's type inference algorithms, Pearl's causal networks are just a few examples, some of which we will encounter in this course.

[^algorithms]: We have seen many examples in Algorithm Analysis. This semester we will learn about parsing, type inference, interpretation and verification.

[^problemsolving]:  Examples: Geometry, differential equations, recursive functions, dynamic programming, probability, information theory, backtracking, invariants, lock-free data structures, concurrency, Bayesian reasoning, causality, ...

## Skills we will learn

**Practice**:
    - programming languages: Haskell, Latex, Blockly, JavaScript, ...
    - software tools: version control (Git), parser generator (BNFC) , debuggers, ...
    - writing a professionally looking report

**Theory**: models of computation, context-free grammars, lambda calculus, rewriting theory, type theory, Hoare logic, ...

**Problem Solving:** minimal interesting example, invariants, ...


