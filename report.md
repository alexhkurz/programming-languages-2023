# Report

Instead of a midterm and final exam, you will write a report. The report is individual work. Think of it as a take home with several deadlines throughout the semester (but see also the Teaching Rationale below).

Out of a total of 200 points the report is worth 60 = 12 * 5 points and divided approximately up as follows (details may change with the number of weeks that have homework).

The report is inividual work.

- (12x2=24 points): Homework with solutions to technical exercises to be done every week  along side the lectures.
- (12x2=24 points): Build a narrative around the technical homework that shows how the technical work fits into a wider context.
- (6 points): Layout, typesetting. 
- (6 points): Lessons from the Project. 

The report will be written in Latex. A template for your LaTeX source file [`report.tex`](report/report.tex) and the compiled [pdf](report/report.pdf) and do *not* change the layout settings such as spacing of lines and fontsize. See also [`latex-example.tex`](report/latex-example.tex) and [`latex-example.pdf`](report/latex-example.pdf).

- You will keep both `report.tex` and `report.pdf` in a personal private Github repository. Respect my naming conventions, including upper and lower case (I may use scripts to download your files). 
- Unless specified otherwise your repo should only contain the following files
    ```
    .gitignore
    report.pdf
    report.tex
    README.md
    ```
    The readme should contain name and email.
- For example, if a homework requires programming, make a subdirectory `src` where you store the relevant program files; if you want to include images in your report, make a subdirectory `img`; etc.
- Always use the same repository for all submissions of the course (get in touch if you think an exception is appropriate).
- Do not name different versions of your report, instead rely on the version control of git.
- Give me access to your private github repo by inviting me as a collaborator (my GitHub name is alexhkurz).
- Submission info below.

## Homework

Think of the homework part of the report as a semester-long take home exam. It is your responsibility that at the end of the semester your answers are correct. 

**Deadlines:**
- Weekly deadlines. Most weeks have 2 points upon completion (approx 24 in total). 
    - To make your reports more self-contained, state the question before you give your answer.
    - Deadline are Sunday at midnight. This hopefully gives me time to look over your answers on Monday. 
    - I will discuss the questions Tuesday in class and you will have an opportunity to improve your answers. 
- Final deadline. The remaining points (approx 24) are awarded after the final deadline at the end of final examination week:
    - You are expected to improve upon your weekly submissions for the final version of the report. 
    - Add to each technical homework a narrative. If a question consisted in performing an algorithm, explain how the algorithm works. Also explain the technical work in a larger context. Now, at the end of the semester is a good time to show that you understand the big picture.
- The full report is due on the Friday of final examination week. 

**Submission:**  After completing the homework, each weak, **submit the URL of the pdf of the report in your private github repo via Canvas** before the deadline. Make sure that you share access to your private repo with me.

Let me say again: Do not keep different copies of your report in the repo. I will use git to verify timestamps and git's version control if I need to go back to an earlier version.

<!--
## Paper

The paper will go into depth in one topic discussed in class. For a grade A paper, it is essential that you do your own research and go substantially beyond what we do in class. The standard topics are the following:
- Parsing.
- $\lambda$-calculus.
- Rewriting.
- Operational and/or denotational semantics.
- Verification.

Some writing tips:
- Justify your claims by citing appropriate references in the text. Do not just cite a whole book, but be more specific about which page, or which theorem, or which quote is relevant. Add a list of technical references at the end. I would expect at least 5 references that have not come up during the course.
- A rule of thumb is that every paragraph should have one main idea. Do not write long "stream of consiousness" style paragraphs. 
- Make good use of signposting. It should always be clear to the reader where in your overall argument they currently are. Feel free to use subsubsubsections if appropriate.
- In technical writing it is usually a good idea to avoid words such as "very", "immense", "in depth", "comprehensive", etc
- Every paper should have an introduction and a conclusion. 

Recommended length is 5 pages (in the format given in `latex-example.tex`).
-->

## Layout and Typesetting

Good layout and typesetting [^goodLayout]
- makes it as easy as possible for the reader to navigate a document and extract the relevant information; 
- is uniform and even in the sense that it is free from distracing details and "same function" is represented by "same form" (["form follows function"](https://en.wikipedia.org/wiki/Form_follows_function) ... btw, software engineering has a lot to learn from architecture, see for example [Christopher Alexander](https://en.wikipedia.org/wiki/Christopher_Alexander) who influenced the design of Wikipedia, design patterns in object oriented programming, agile software development, and the [REST](https://en.wikipedia.org/wiki/Representational_state_transfer) architectural style underlying much of the WWW).
- takes a lot of time polishing the content and how it is presented[^polishing].

Some concrete examples following from the general principles above:

- Adapt my template in a meaningful way. Don't leave empty sections or trailing "..." in the paper.
- The table of contents should have clickable links (hyperrefs).
- Make sure that the way you use whitespace adheres to the principle of form follows function. In particular, there should be no pages that are largely empty.
- If you use pictures of handwritten solutions, make sure  
    - that your handwritten solutions also show a nice layout and typesetting and that they fit together in style across the whole report;
    - (if you use paper with lines) that your writing fits into the lines;
    - the pictures show no distracting details such as your fingers, or irrelevant background, or the shadow of your phone, etc;
    - that you do not put the first draft of your answer in the report, rather carefully rewrite your draft answer to eliminate potential mistakes.[^firstDraft]

Finally, do not change the general layout specified in the template. Do not change the margins or the line spacing. In case of doubt get in touch. 

[^goodLayout]: I don't want to consider myself as overly strict when it comes to good typesetting, but my experience from grading is that most students produce reports without paying enough attention to layout. 

[^polishing]: Note that the human eye is very good at spotting small visual inconsistencies related, for example, to the use you make of space on the page. While these inconsistencies are not relevant to the content, they are distracting from the content, since they consume some attention of the reader. In other words, they are relevant to the relationship you as an author are trying to build with the reader.

[^firstDraft]: Mathematics is rather similar to programming in this respect. The first draft is (almost) never correct.

## Lessons from the Project

On 3 pages you describe lessons you learned from the group project. Be as technical and detailed as possible. I am mainly interested in examples where you connect concrete technical details with interesting general observations. I am also interested in examples in which the theory discussed in the lectures helped with the design or implementation of the project.

I recommend that you write this while working on the project. This is less than half a page per week and the material should come from the work you do anyway on the project. Just keep your eyes open for interesting lessons while working on the project.

## General Remark on Grading

Grading should not be checking boxes. But as long as students want guidelines, grading will always involve some checking of boxes. So please make sure that you take on board the points I listed above. Some will seem minor to you (like the ones on typesetting) but experience shows that work that has more carefully crafted form typically also has better content (for example, while improving the typesetting of your report, you will likely spot actual mistakes in the math).

## Teaching Rationale

Self-explaining can have a profound effect on problem-solving, in particular during early knowledge acquisiont. In particular, self-explanation helps the learner to build their own mental model and to revise it over time. 

See Chapter 6 of Craig Barton, How I Wish I’d Taught Maths, and references therein.
