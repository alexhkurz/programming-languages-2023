# Participation

Participation is feedback from students that helps me to improve my course. Since this is potentially very subjective, I am refining this below to make the award of participation points evidence based. 

Participation points are available for the following.

- Attending a Fowler School of Engineering Seminar.
- Attending office hours.
- Acting as the weekly representative of a group project.

To make it transparent how participation points are awarded, there must be a document giving evidence of the value contributed by participation. 

- If you attend an Engineering Seminar, write a discussion post on Canvas (approx 200 words) summarizing the key content of the seminar and adding a question of your own.
- If you attend office hours come prepared with an interesting question (about anything related to the course from a technical question to homework or the lectures to the ramifications of programming languages technology on the wider society). After the OH, write me an email summarizing briefly the key content of what we discussed.
- If you act as a group representative, present the work of your group in office hours and write an email to me and the group summarizing the discussion. Each week 2 students can join together and both get the participation points.

This must be submitted **inside 24 hours**.

I also reserve the right to award participation points for other remarkable contributions.