# Assignment 2 

This assignment comes in 4 parts and a critical appraisal. There are 30 points availble.

## Introduction

The purpose of the assignment is to build a simple functional programming language that has function definitions, function application, numbers, conditionals, recursion and lists. We start from lambda calculus, which already has function definitions and application.

For info about LambdaNat see the lectures.


## Instructions

You may form groups again. Easiest for me is if you keep working **in the same repo as before** (if that is not possible get in touch). 

I made a new repo [LambdaNat2023](https://codeberg.org/alexhkurz/LambdaNat2023) for the assignment. Copy the folder [`LambdaNat`](https://codeberg.org/alexhkurz/LambdaNat2023/src/branch/main/LambdaNat) into your repo:

    YourRepo
        Assignment1 -- contains your work on Assignment 1
        Assignment2
            README.md -- group members, your critical appraisal
            LambdaNat 

**You need to keep my naming conventions as I will run scripts to test your software.** As in Assignment 1, also for this assignment my naming conventions are part of the specifications. 

## General Remarks 

### Rationale

We are aiming for the simplest possible interpreter of the simplest interesting functional programming language. Amongst many other simplifications, the language is untyped, the implementation is not optimized for efficiency, and the interpreter does not produce meaningful error messages. 

### Interpreter

For each constructor in the grammar there will be one clause in the definitions of evaluation `eval'` and substitution `subst`

`subst` just applies `subst` recursively to sub-expressions. On the other hand, `eval'` implements the operational semantics of the programming language.

### Debugging

As the interpreter and programs gets more complicated, it becomes more difficult to determine whether unexpected results are caused by the program or by the interpreter. If you run into unexpected behaviour create a [minimal interesting example (MIE)](MIE.md). 


## Part 1 (10 points)

The purpose of this part is to implement an interpreter for the language defined by the grammar `LambdaNat5.cf`. Study `Interpreter.hs` to see what is already implemented and some corresponding tests in `test/test.lc`. Run

```
./build.sh
cabal run LambdaNat test/test.lc
```

The expected output is

```
testing_arithmetic ;; 1 ;; 2 ;; 3 + (\ x . x) ;; (\ x . x) + 4 + 1 ;; testing_conditionals ;; 0 ;; false ;; 0 ;; 0 ;; 0
```

### Arithmetic

```
LE.       Exp9 ::= Exp10 "less_equal" Exp10 ;
Plus.     Exp10 ::= Exp10 "+" Exp11 ;
Minus.    Exp10 ::= Exp10 "-" Exp11 ;
Times.    Exp11 ::= Exp11 "*" Exp12 ;
```

As we have seen, addition is already implemented in `Interpreter.hs`. The other operations work similarly.

Add you own test cases to `test.lc`.

### Lists

```
Hd.       Exp6 ::= "hd" Exp ;
Tl.       Exp6 ::= "tl" Exp ;
Nil.      Exp15 ::= "#" ; 
Cons.     Exp15 ::= Exp16 ":" Exp15 ;
```

Let us look at some of the details.

- According to the rules `Nil` and `Cons` we can build lists such as

        a:b:c:#

    The precedence levels of the grammar are engineered in such a way that `a:b:c:#` is parsed as `a:(b:(c:#))`.

    For the exercise below, recall that abstract syntax is defined in `AbsLambdaNat.hs`, which in turn is generated automatically by BNFC.

    **Exercise:** (not necessary to hand this in, but should help to implement the computation rules for `hd` and `tl` in the interpreter) What is the abstract syntax tree of `a:b:c:#`? Write the answer down on paper. Run `a:b:c:#` in the parser and compare with your pen-and-paper answer.

- `hd` and `tl` are pronounced "head" and "tail", respectively. The first task here is to adapt the interpreter of LambdaNat4 in such a way that head and tail compute as, for example,

        hd a:b:c:#   --*-->   a
        tl a:b:c:#   --*-->   b:c:#

    Note that this does not specify what will happen if your computation reaches `hd #` or `tl #`. One solution is to not have a case for the empty list, raising a "Non-exhaustive patterns" exception at runtime if `hd` or `tl` are applied to `Nil`. Alternatively, you can use Haskell's `error` primitive and return a more meaningful error message, eg you can return `error "hd applied to empty list"`. 

    Here are some further test cases to check whether your interpreter "reduces under a `hd`". 

        hd ((\x.x)(a:#))   --*-->   a
        hd (((\x.x)a):#)   --*-->   a

    Similarly, one should reduce under a `tl`.

    **Hint:** Note that lists are similar to successor numbers. After all, a successor number is essentially just a list of `S`s (and `0` plays a role similar to the EndOfList symbol `#`). 

    **Exercise:** (not necessary to hand this in, but should help to see how to implement the two rules above in `Interpreter.hs`) Translate the computation rule `hd a:b:c:#   --->   a` from concrete syntax to abstract syntax. Also run `hd a:b:c:#` in the parser and compare.

**Further comments:**

- Lists can also be nested in order to form trees as in 

        Plus:(N1:#):(Times:(N2:#):(N3:#):#):#

    If you wonder why we need the EndOfList symbol `#` above, then the answer is that in the tree above it is redundant if we have agreed that the `N` symbols are constants (take no arguments) and that `Plus` and `Times` are binary (take exactly 2 arguments). Then we could write the above instead as 

        Plus:N1:(Times:N2:N3)

    (which, by the way, we can think of as an abstract syntax tree for `1+2*3`.) The reason we need the EndOfList symbol is that lists are meant to work in situations where we do not know at *programming time* (aka compile time) how long the lists will be at *run time*. 

- The previous remark hides a deeper duality between two different readings of `#`, one as the empty list (often written as "nil") and the other has the EndOfList symbol. This duality is known as the duality of **algebras** and **co-algebras**. In the algebraic view, we *build* or *construct* finite data from smallest data (here `ENil` with concrete syntax `#`) by inductively applying a finite number of rules (here `ECons` with concrete syntax `:`). In the co-algebraic view, data is potentially infinite and we *observe* or *deconstruct* this data (here using `hd` and `tl`) until we get to the end symbol (here `#`).

- As common in functional programming I designed the grammar so that you can drop many parentheses. But this also means that you need to watch out carefully where parentheses are needed. In case of doubt you can always put them in, for example `(hd one:two:#):#` abbreviates `(hd (one:(two:#))):#`. A good way to learn where parentheses go is 
- to look at the precedence levels in the grammar,
- to put in enough parentheses so that the program runs in the expected way and then to eliminate all unnecessary parentheses two by two.

### Definitions and Recursion: `let` and `let rec`

See my writeup on [`let`](https://hackmd.io/@alexhkurz/rJEeYqZtw#Naming-functions-using-let) and [`let rec`](https://hackmd.io/@alexhkurz/rJEeYqZtw#Recursion).

## Part 2: Write LambdaNat5 programs (7 points)

Now we have our own programming language, LambdaNat5, it is time to write some programs in that language. Remember that the syntax of the languages is defined in `grammar/LambdaNat5.cf` and the operational semantics is as defined in `src/Interpreter.hs`. For example programs see `test/test.lc`.

- You will provide in `test/solutions.lc` the functions

        length
        sum
        sort

    Running 
                
        cabal run LambdaNat test/solutions.lc 
    
    should produce the correct outputs as described in the test cases below. **Indicate further tests in comments.** (For example, all branches of an if-then-else should be covered by some test.)

#### `length`

`length l` evaluates to the length of a list `l`.

Test case:

```
length a:b:c:#    --*-->    3
```

#### `sum`

`sum l` sums up a list of integers, for example

```
sum 1:2:3:4:#      --*-->   10   
```

#### `sort`

`sort l` sorts a list of integers using insertion sort. Use the following pattern:

```
 let rec insert = ... 
 in
 let rec sort = ...
 in
 sort (5 : 3 : 4 : 3 : 1 : #)
 ```

## Part 3 (3 points)

Implement the functions of Task 2 also in Haskell and add them to a file `test/solutions.hs`. This file should have a `main`-function that prints the results of the same test cases specified above. 

## Part 4 (5 points)

Add a feature to the grammar and the interpreter.

It could be, for example, a new arithmetic operator.
## Critical Appraisal (5 points)

(approx one page in README.md, take care of good layout and typesetting )

There are a lot of interesting questions. For example, `#:#` does not parse, but `(#):#` does. Why? Does that mean that it would make sense to change the grammar? 

As you spent quite a lot of time on this assignment, I would like to know about the questions you came up with. For full points, your critical appraisal needs to contain **personal interesting** observations and/or questions.

In addition, also discuss the following.

- Reflect on the differences between LambdaNat5 and the Calculator. In LambdaNat5, why can't we implement arithmetic using the simple
    ```
    eval' (Plus e1 e2) = (eval' e1) + (eva' e2)
    ```
    similar to what we have done in the calculator? Are `+,-,*` implemented using call by name or call by value? What could be a reason for this choice?

- Reflect on the differences between LambdaNat5 and Haskell. In your experience from this assignment, how does writing code in LambdaNat5 and Haskell compare? How far did we come in implementing a functional programming language? What is missing? What can you say about how one should go about extending LambdaNat5 to a more powerful functional language (such as Haskell)?

- Motivate the feature you added to LambdaNat5. Explain how it works and how you tested it.
