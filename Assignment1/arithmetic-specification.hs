-- A Virtual Machine for Unary Arithmetic (UAVM) 
-- Copyright: Alexander Kurz 2023

-----------------------
-- Data types of the VM
-----------------------

-- Natural numbers
data NN = O | S NN
  deriving (Eq,Show) -- for equality and printing

-- Integers
data II = II NN NN
  deriving Show -- for printing (define your own equality below)

-- Positive integers (to avoid dividing by 0)
data PP = I | T PP
  deriving (Show, Eq)

-- Rational numbers
data QQ =  QQ II PP
  deriving Show

------------------------
-- Arithmetic on the  VM
------------------------

----------------
-- PP Arithmetic
----------------

-- add positive numbers
addP :: PP -> PP -> PP 

-- multiply positive numbers
multP :: PP -> PP -> PP

---------------
-- TYPECASTINGS
---------------

-- cast numbers of type PP as numbers of type NN
nn_pp :: PP -> NN

-- cast numbers of type NN to numbers of type II
ii_nn :: NN -> II

-- cast numbers of type PP to numbers of type II
ii_pp :: PP -> II

----------------
-- NN Arithmetic
----------------

-- add natural numbers
-- 0 + m = m, (n+1) + m = (n+m)+1
addN :: NN -> NN -> NN
addN O m = m
addN (S n) m = S (addN n m)

-- multiply natural numbers
-- 0 * m = 0, (n+1) * m = (n*m)+m
multN :: NN -> NN -> NN
multN O m = O
multN (S n) m = addN (multN n m) m

-- division, eg 13 divided by 5 is 2 
divN :: NN -> PP -> NN

-- remainder, eg 13 modulo by 5 is 3
modN :: NN -> PP -> NN

-- greatest common divisor, eg gcd 9 33 is 3
gcdP :: PP -> PP -> PP

----------------
-- II Arithmetic
----------------

-- Addition
-- (a-b)+(c-d)=(a+c)-(b+d)
addI :: II -> II -> II

-- Multiplication
multI :: II -> II -> II

-- Negation
negI :: II -> II

-- Equality of integers
instance Eq II where
  (II a b) == (II c d) = <insert your code here>

----------------
-- QQ Arithmetic
----------------

-- Addition
addQ :: QQ -> QQ -> QQ

-- Multiplication:
multQ :: QQ -> QQ -> QQ

-- Equality of fractions
instance Eq QQ where
  (QQ a b) == (QQ c d) = <insert your code here>

----------------
-- Normalisation
----------------

normalizeI :: II -> II

----------------------------------------------------
-- Converting between VM-numbers and Haskell-numbers
----------------------------------------------------

-- Precondition: Inputs are non-negative
nn_int :: Integer -> NN

int_nn :: NN->Integer

ii_int :: Integer -> II

int_ii :: II -> Integer

-- Precondition: Inputs are positive
pp_int :: Integer -> PP

int_pp :: PP->Integer

float_qq :: QQ -> Float
-- use fromIntegral to convert from Integer to Float

------------------------------
-- Normalisation by Evaluation
------------------------------

nbe :: II -> II

----------
-- Testing
----------

main = do
    putStr "addN:  "; print $ int_nn (addN (nn_int 3) (nn_int 4)) -- 7 
    putStr "multN: "; print $ int_nn (multN (nn_int 3) (nn_int 4)) -- 12
    -- design your own tests and add them to main 
    -- make sure that all relevant functionality is tested
    -- add expected test results as comments
    -- do not change the names of the functions in the specification, I will use them for testing
    