# Assignment 1 

The main purpose of this assignment is to learn the programming technique of recursion over algebraic data types. We will use algebraic data types to implement abstract syntax. And recursion over abstract syntax is the basis of all modern interpreters, type checkers, and compilers. As a programming technique it is not specific to Haskell, but programs written in this style are more elegant, concise, readable, maintainable when written in a functional language rather than an object oriented language. (If you know Java you may want to also do the assignment in Java and compare for yourself ... let me know if you do this.) In fact, LISP, the first mainstream functional programming language, was invented together with abstract syntax.

There is a lot to learn in this assignment, I expect to see many of you in office hours with questions.

The assignment will be implemented in Haskell. ***Prerequisites: Lectures 1.1 - 2.2.***

## Administrative Preliminaries

- Read carefully my general notes on [assignments](../assignments.md).
- In the private repo you use for the assignments, make a folder `Assignment1`.
- **Do not rename the functions in my specification.** I will test your programs using the names (and types) of the functions as specified.

## Proper use of git will be taken into account for grading

- The only files in your repo should be the following.

        README.md -- names of group members
        Assignment1
            README.md -- critical appraisal
            arithmetic.hs

- Commit each time, with *meaningful messages*, after you implemented and *tested* a function in `airthmetic.hs`.

- If several group members work independently on the same file, make use of the git capabilities called branches and merge.

## Submission 

Submit the URL of `arithmetic.hs` in your repo via Canvas.

The aim is to practice programming by recursion over algebraic data types (which is the main technique we will use to write interpreters) and to extend the virtual machine from the lectures.

(The following description got quite long, but it is not as bad as it looks at first sight. Most functions have an implementation with only one or two lines of code.)

## Testing

The `main` function in [`arithmetic-specification.hs`](arithmetic-specification.hs) contains some tests, but you should add your own tests. **Each line of code needs one test.** All your tests should be executed automatically upon `runhaskell arithmetic.hs`.

I recommend **test driven development**: First write the test, then the corresponding line of code, then test. This helps making sure that you have the specification that the code should fulfill clearly in your mind.


## Instructions

- Study the specification [`arithmetic-specification.hs`](arithmetic-specification.hs). The aim is to implement all functions. 

    (The specification does not compile because it contains function declarations that are not implemented. Compilation (try it) will give you `"lacks an accompanying binding"` error messages. There is no need to put `arithmetic-specification.hs` in your repository.)
- Do not use Haskell's built-in arithmetic. The point of this assignment is to implement our own arithmetic from first principles.
- You will implement functions converting between Haskell-numbers and VM-numbers. Only use them for normalization-by-evaluation (see below) and testing.
- Start from the template file [`arithmetic.hs`](arithmetic.hs). You should be able to compile and run it with `runhaskell arithmetic.hs`. 

    (Aside: Use `ghc` if you want to compile only and use `runhaskell` to compile *and* run. Use `ghci` for interactive mode where you can load the file using `:load arithmetic.hs`.)


### Summary

The overall idea is to explore how to extend our implementation from the lectures of unary natural numbers 

```haskell
data NN = O | S NN
```

to integers and fractions. We will detail this in the following.

### Integers

The idea is that we can use pairs `(n,m)` of natural numbers to represent the integer `n-m`. In Haskell we define a type `II` (to remind us of "Integer") as an algebraic data type with two components of type `NN`: 


```haskell
data II = II NN NN
```

**Remark:** This way of defining negative numbers has the advantage that it keeps the logic as simple as possible. Moreover, this trick also works whatever presentation one chooses for natural numbers (unary, binary, decimal, etc). For binary numbers one has the more efficient, but also more complicated, [two's complement](https://en.wikipedia.org/wiki/Two%27s_complement).

For the assignment, the task now is to implement some familiar arithmetic operations directly on such pairs of numbers.

**Remark:** `NN` is a recursive data type (why?) and functions of type `NN->...` are defined by recursion on `NN` while `II` is not a recursive type and functions on `II` are typically not recursive functions.

### Fractions

Simlarly, a fraction can be represented as a pair `(numerator,denominator)` where the numerator is an integer and the denominator is a positive integer. So we first define a data type of positive numbers (positive numbers start at 1, not 0)

```haskell
data PP = I | T PP
```

and then a type of fractions as an algebraic data type with two components, a numerator of type `II` and a denominator of type `PP`

```haskell
data QQ = QQ II PN
```

Since Haskell is strongly typed and does not have subtyping, a number of type `PP` is always different from a number of type `NN` or a number of type `II`, so we need functions such as `ii_pp :: PP -> II` that do the *type-casting*.

### Equality

If you want to use the equality test `==` on a user-defined data type, you can ask Haskell to derive the code for the equality test automatically. This is what happens here (`Eq` for equality, `Show` for printing):

```haskell
data NN = O | S NN
  deriving (Eq,Show) 
```

But sometimes, the automatically derived equality test is not the one we want. Then you can define your own equality test:

```haskell
instance Eq II where
  (II a b) == (II c d) = <insert your code here>
```

For more background, I recommend Learn You a Haskell on [Types and Typeclasses](http://www.learnyouahaskell.com/types-and-typeclasses).

### Normalization

You may have noticed that the same integer can be represented in different ways, for example `(S (S O), O)` and `(S( S (S O)), S O)` both represent 2. But `(S (S O), O)` is special in the sense that both components are as small as possible. Such special forms are often called normal forms (they will play an important role later in the semester) and the process of converting data into normal form is called **normalization**.

Using recursion, write a function

```haskell
normalizeI :: II -> II
```

that converts and number of type `II` into its normal form.

### Converting Numbers

Writing and reading larger successor numbers is tedious. In particular for testing the VM, it is convenient to have functions that convert between Haskell-numbers and VM-numbers. 

For example, we want to have

```haskell
nn_int :: Integer -> NN
int_nn :: NN->Integer
ii_int :: Integer -> II
int_ii :: II -> Integer
pp_int :: Integer -> PP
int_pp :: PP->Integer
float_qq :: QQ -> Float
```

### Normalisation by Evaluation

Instead of normalising as above by recursion on `NN` one can also normalise by evaluation, that is, by converting an `II` to a Haskell integer and then back to an `II`. Write a function

```haskell
nbe :: II -> II
```

that implements this strategy.


### Further Hints and Remarks

- *Hint:* Spend some time on thinking about the logic behind each function. Once you have the idea, the solution will be short. Most of the functions are one- or two-liners.

    For example, the two Haskell equations

    ```haskell
    multN :: NN -> NN -> NN
    multN O m = O
    multN (S n) m = addN (multN n m) m
    ```
    correspond to the mathematical equations

    0 * n = 0  
    (1+n) * m = n*m + m

    I recommend that you engineer your function by reversing this process. Think first of the math and then about how to code it.

    **Document your functions by add these equations in comments to your code.** (This does not apply to the functions that convert between different types.)

    How to encode positive numbers using `I` and `T` is discussed in the next hint.

- *Hint:* Mathematically, below, all entries on the same line  represent the same number.

    `Int` | `NN` | `PP` | `II` | `QQ`
    |:---|:---|:---|:---|:---|
    | -1  | | | `II O (S O)` | `QQ (II O (S O)) I`
    | 0 | `O` | | `II O O` | `QQ (II O O) I`
    | 1/2 | | |  | `QQ (II (S 0) O) (T I)`
    | 1| `S O` | `I` | `II (S O) O` |  `QQ (II (S 0) O) I`
    | 2 | `S (S O)` | `T I` | `II (S (S O)) O` | `QQ (II (S(S 0)) O) I`
    | 3 | `S (S (S O))` | `T(T I)` | `II (S (S (S O))) O` | `QQ (II (S(S(S 0))) O) I`

    But in Haskell **these data types are disjoint**. For example, if you input an `I` or a `1` to a function that expects data of type `NN`, you will get an error message.

- *Hint:* Data type definitions such as 
    ```haskell
    data NN = O | S NN
    data PP = I | T PP
    ```
    are pure syntax and do not mean anything on their own. Note that both `NN` and `PP` have exactly the same structure. I chose `O` and `S` to indicate their intended meaning (0 and Successor). Similarly, `I` and `T` should remind us of 1 and +1. But this is purely conventional.

- *Hint:* Separate clearly in your mind syntax from semantics (=meaning): Syntactically, `O` and `I` are just symbols. The meaning of these symbols only arises from the operations on these data. For example, if we write a function

    ```haskell
    addN O n = n
    ```

    this is consistent with our interpretation of `O` as `0`, because we know that $0+n=n$ in mathematics. On the other hand, if we simply transferred this idea from `NN` to `PP` writing

    ```haskell
    addP I p = p 
    ```

    then this would *not* be consistent with `I` meaning `1``, because $1+p=p$ is *not* a valid  equation of arithmetic.

- *Hint:* The data type `PP` was introduced to make sure that a fraction never has a denominator of 0. But there are other places where you may allow runtime errors, for example, the function `nn_int` is not defined for negative integers. Alternatively (and better), you can proceed as in item 1 of [8 ways to report errors in Haskell](http://www.randomhacks.net/2007/03/10/haskell-8-ways-to-report-errors/). I marked these places in the `arithmetic-specification.hs` by writing down the precondition that will guarantee that the function does not throw a runtime error.

- *Hint:* Using GitHub copilot (or some other AI) will make your task much simpler. But be careful, when I tried this, not all suggestions by the AI were correct. 

- *Hint:* Some functions may need auxiliary functions. Auxiliary functions also need to be tested and documented by their corresponding mathematical equations.

