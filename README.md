# programming-languages-2023

(under development ... more teaching materials to be added in due course)

https://codeberg.org/alexhkurz/programming-languages-2023/

- [Invitation](invitation.md)

- Quick links

    - [Lecture by Lecture](lecture-by-lecture.md)  
    - [Syllabus](syllabus-long.md)
    - [Report](report.md)
    - [Projects](projects.md)
    - [Git best practices](git-best-practices.md)

- Further Reading   

    - Blogs on programming languages and software engineering:
        - [Cory Doctorow](https://pluralistic.net/2023/12/06/privacy-first/#but-not-just-privacy)
        - [LambdaClass Blog](https://blog.lambdaclass.com/?ref=blog-lambdaclass-newsletter), distributed systems, machine learning, compilers, operating systems, security and cryptography.
        - [loge.hixie.ch](https://ln.hixie.ch/) by Ian Hickson: [Reflecting on 18 years at Google](https://ln.hixie.ch/?start=1700627373&count=1), Nov 2023.
        - [Lehman’s Laws of Software Evolution and the Staged-Model](https://learn.microsoft.com/en-us/archive/blogs/karchworld_identity/lehmans-laws-of-software-evolution-and-the-staged-model )
        - Joel Spolsky, [Making Wrong Code Look Wrong](https://www.joelonsoftware.com/2005/05/11/making-wrong-code-look-wrong/), 2005. A classic blog on, yes, "making wrong code look wrong". [Distributed Version Control is here to stay, baby](https://www.joelonsoftware.com/category/reading-lists/rock-star-developer/) is also interesting, partly *because* it is over 10 years old.
        - Javier Chavarri, [Data-first and data-last: a comparison](https://www.javierchavarri.com/data-first-and-data-last-a-comparison/). The article explains how the data-first and data-last style interact with type-inference.
        -  Andrei Taranchenko, [Death by a Thousand Microservices](https://renegadeotter.com/2023/09/10/death-by-a-thousand-microservices.html), 
    - Blogs on software and society:
        - ...
    - Open Source Software Projects:
        - [Catala](https://github.com/CatalaLang/catala#readme) is a programming language for programming of the law.
        - [Supabase](https://supabase.com/blog/supabase-realtime-multiplayer-general-availability) is provides realtime database access in distributed systems.
        - [Heisse Preise](https://github.com/badlogic/heissepreise) is an API that can be used to scrape prices of grocery chains for data analysis.
    - ["Must Reads"](must-reads.md)  
    - [History of Programming Languages](history.md)  

- [Acknowledgements](acknowledgements.md)  

