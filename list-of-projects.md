list-of-projects.md

- Jared, Maverick, Alex, [LatexPy](https://github.com/amaraljt/LaTeXPy/blob/main/design.md)
- Peter Smith [BitFielder](https://github.com/pesmith0/CPSC_354_DSL_BitFielder/)

- Tony Bautista, Aidan Lewis-Grenz, Jordan Silver, [BlocklyBiology](https://github.com/ThomasKim13/GP354)
- Iliana Chae, Andrew Kwon, Jaden Suh, [Chemistry DSL](https://github.com/ilianachae/CPSC354-DSL)

- Brynn, Kayla, Sreya, Zakari, [NotionWeb](https://github.com/brynnmcgovern/NotionWeb)
- Ethan Clunie, Daniel Dinh, Mason Li, Max Starreveld, [ROMSly](https://github.com/ddinh0411/ROMSly)
- Carlos Garcia, Emma Harper, Emily Nguyen, Sofia Bejerano, [TaskFlow](https://github.com/YallowMan/TaskFlow)

- Michael, Marco, [Hydroponic](https://github.com/cdnmonitor/hydroponic-scheduling)
- Thomas Jordan, Mason Moore, Thomas Kim https://github.com/masmoore/ProgrammingLanguagesGroupAssignments

- Jason, Ori, Liam, Miles [RCCar](https://github.com/jasonbohlinger/cpsc354RCCar)
- Drew Bozarth, Alan Lu, Karen Ngo, Thomas Ogawa, [Boston Dynamics](https://github.com/kango20/Boston_Dynamics_Dogs_API)
- Parker Escallete, [Pulse Robot Automation](https://github.com/pescalette/pulse)

- Josh, Rahul, Shree, Dylan, [RBlockly](https://github.com/shmurthy08/RBlockly)
- Madi Schultz, Austin Simpson, Diego Lopez Ramos, Tyler Kay [DataViz](https://github.com/madischultz/DataViz)

- Lucas Gaudet, Lawrence Leymarie, [BlocklyQL](https://github.com/L-Gaudet/BlocklyQL)
- Sophia Guarnotta, Esha Yamani, Ponthea Zahraii, [ScratchQL](https://github.com/eshayamani/ScratchQL) 

- Natalie Huante, Kyle Wynne, Lauren Szlosek [HealthBlocks](https://kylewynne.github.io/Health_Blocks/)
- Christopher Isidro, Spencer Lafferty, Justin Lee, Patrick Polcuch [Language for Music](https://github.com/PatrickPolcuch/CPSC354Project)
- Viviana, Suparna, Katherine, Pelin, Catherine [EcoCode](https://github.com/vvanaa/EcoCode)

- Luis Rivas, Kevan Parang, Ivan Orlovic, Peter Senko [FitnessPlanner](https://github.com/Lnrivas/CPSC_354_Project)
- Madi Tansy, Max Miller, Sanil Doshi [Home-Automation](https://github.com/mtansy/Home-Automation-DSL)

- Apoorva Chilukuri, Briana Craig, Cece Hagen, Michelle Zhang, [FinPlan](https://github.com/michelleezhangg/FinPlan)
- Evelyn Lawrie, Alex Rea, Chris Ton-That, Minna Yu, [BlocklyML](https://github.com/elawrie/BlocklyML)
- Olivia Chilvers, Kaitlyn Vetica [Student Program Evaluation Generator](https://github.com/odchilvers/chilvers-vetica-group-project-CPSC354)

- Jose Arellano


## Midterm Presentations

24 talks, 6 hours of lectures, 4 presentations

10 minutes

- There should be an introduction motivating the project and explaining why it is interesting.
- Some details on design, specification and implementation. 
- Demo.
- Planning.



