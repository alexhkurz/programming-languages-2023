# Projects

The aim of this group project (4 students, same group as the one for assignments, groups can have students from different sections) is to build an open source software product that solves a real world problem and makes essential use some of the main topics of this course such as parsing, interpretation/compilation, verification.

On the way students will learn to participate in open source software development, including  the proper use of git and code review.

The project is worth 80 + 20 points (50%).

## Topics

The project needs to have as a major component an area central to the course Programming Languages: parsing, interpretation/compilation, verification. The default project is to develop a domain specific language (DSL) using [Blockly](https://developers.google.com/blockly). To get an impression of what can be done with Blockly have a look at [Scratch](https://scratch.mit.edu/).

- Choose a domain that interests you.
- Design a domain specific language.
- Implement the DSL using the Blockly Api.

Blockly presents a powerful interface intermediate between a GUI and a general purpose text-based language. **So the tasks you want your DSL to encode should be more complicated that what can be achieved by a menu-driven UI.**


## Timeline

Timeline of deliverables and deadlines (measured in weeks): Description (2), Specification (4), Prototype (7) with Midterm Presentations in Week 8, Product (12), Final Presentations in Week 14, Final Repository (15).

## Grading Guidelines 

(updated for 2024)

I run a Blockly project for the first time. These are rough guidelines that may be refined later.

A: As for B. Moreover, the whole project is in excellent shape and can be recommended as a starting point for future developments. - - - Interesting and surprising references; . 

B: An open source project that runs without any problems, is very well documented and tested, and demonstrates a good understanding of the learning outcomes of this course. - - - A good number of relevant references going beyond what was discussed in class; ... 

C: An open source project that runs without major problems, has helpful documentation and demonstrates an adequate understanding of the learning outcomes of this course. - - - Some basic references; ...

<!--
## Midterm Presentation (Week ...) 

## Final Presentation (Week 14)

The presentation will take up some of the points relevant for the final submission (detailed in the next section):

- There should be an introduction motivating the project and explaining why it is interesting.
- There should be a literature review, references to related work and theoretical background.
- Some details on design, specification and implementation. 
- The role of data structures and algorithms in the project. Describe the algorithms involved in some detail, in particular if you designed and implemented them yourself.
- How was the work divided between group members? Who was responsible for what?
- Some details on results and achievements.
- Directions for future development.


## Final Submission of the Repository

(taken from Algorithm Analysis 2023 ... to be adapted)

Here are some points I will take into account when grading the projects.

- The project must be open source and on a public git repository. It must contain a file called LICENSE such as, for example, the [MIT License](./LICENSE). It also must contain a file `.gitignore`. See also [Git best practices](git-best-practices.md). 
- The repository must be structured in a way that makes it as easy as possible for a reader to access the relevant information. This includes proper use of markdown syntax.
- There should be an introduction motivating the project and explaining why it is interesting (a good project has a convincing narrative).
- There should be a literature review, references to related work and theoretical background (a good project describes the wider context in which it is situated). References are more useful when it is clear how they relate to the specifics of the project (just "dumping references at the end" is not useful).
- The readme must contain a description of how to deploy and run the software.
- The code must run, be well commented and documented. 
- The documentation should include, for example (adapt as appropriate):
    - What components does the software consist of? How do components interact?
    - What programming languages and APIs are used?
    - What data structures and algorithms did you implement?
    - How was the work divided between group members? Who was responsible for what?
- The documentation must be detailed enough so that it is possible to understand how the code works without having to read the code.
- **Beware of Plagiarism:** Make sure that if you took some code from somewhere you make clear, both in the code and in the documentation, from where you took it.
- Give details of how the software was tested. Most projects should have code that was used for testing. Provide the tests that have been written, as well as a description of how to run the tests and reproduce the test test results. 
- Depending on the project, there may be other ways of validating the software (eg questionnaires, data analysis, and more).
- Describe what works and what does not. Did your plans change? What is left to do?
- Suggest directions for future development. Ideally, a list of possible extensions is described and designed, including details of how the current code base should be modified for the extension.

The last point is particularly important to me. A good open source project is one that inspires others to take it further and provides a basis for future developments. I will also judge your repo on **whether your repo will be a good starting point for future open source development by other developers**.
-->
