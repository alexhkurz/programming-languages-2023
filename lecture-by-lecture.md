
# Lecture by Lecture

**Quick Links**: [Installing Haskell](https://hackmd.io/@alexhkurz/Hk86XnCzD), [Videos](videos.md), [Assignment 1](Assignment1/assignment1.md), [Calculator](https://codeberg.org/alexhkurz/Calculator), [Programming with Aider](https://codeberg.org/alexhkurz/blockly-experiments/src/branch/main/README.md), [BlocklyLambda](https://github.com/alexhkurz/BlocklyLambdaCalculus), [A Lambda Calculus Interpreter](https://codeberg.org/alexhkurz/LambdaCalculus), [Feedback on Projects](feedback-projects.md), ...

## Functional Programming and Recursion (Haskell)

We need a programming language that makes it as simple as possible to implement interpreters. Choice of language always is a compromise. There are good arguments (which will become clear during the course) why a functional programming language such as Haskell is the best choice for implementing interpreters (and compilers).

- L 1.1: *In which we encounter **functional programming** and run through an online Haskell tutorial.*
    - Organization of the course (see syllabus).
    - [First Haskell Steps](https://hackmd.io/@alexhkurz/SJgHGZ_nw). We spent most of the time with the online tutorial at [`tryhaskell.org`](https://tryhaskell.org/).
    - Homework until Thursday: (1) Install Haskell. After installing Haskell you should be able to use `ghci` to run an interactive Haskell session similar to the one we have done in class, but now not in the browser but in your terminal. (2) Set up a private Github repo, invite me as a collaborator. (3) Watch the first 8 videos of the [learnyouahskell series](https://www.youtube.com/playlist?list=PLwiOlW12BuPZUxA2gISnWV32mp26gNq56). Use the first four chapters of [learnyouahaskell](http://learnyouahaskell.com/chapters) as a reference. (4) Prepare questions that we can discuss in class on Thursday.
    - Start thinking about the project: What domain do you find interesting? With whom do you want to collaborate? 

- L 1.2: *The computational model of functional programming (the **Rewriting Machine**) is simpler than the computational model of imperative programming.* 
    - [Imperative vs functional programming](https://hackmd.io/@alexhkurz/Sy_uAuaTh). See also my video on [The computational model of functional programming](https://youtu.be/u_OMwv8tDVg).
    - [Greatest Common Divisor](https://hackmd.io/@alexhkurz/r1UdeMAT3). 
    - [Recursive Programming in Haskell](https://hackmd.io/@alexhkurz/By40zt663).
    - Homework until Tuesday:
        - Watch videos:
            - 9-15 of the [learnyouahskell series](https://www.youtube.com/playlist?list=PLwiOlW12BuPZUxA2gISnWV32mp26gNq56). Most important for us right now are: 5 Recursion, 6a Higher order functions, 8a Defining types.
        - Read Chapters 5 and 6 of [learnyouahaskell](http://learnyouahaskell.com/chapters).
        - Read Chapter V of [Goedel, Escher, Bach](https://www.physixfan.com/wp-content/files/GEBen.pdf) and make sure to start with the dialogue "Little Harmonic Labyrinth" on page 111 of the pdf (we will see this structure in the next lecture again when we show how to solve the Towers of Hanoi using a Stack Machine). 
    - Homework for the report: [Greatest Common Divisor](https://hackmd.io/@alexhkurz/r1UdeMAT3). I believe the Homework for the report can be done "naively" from first principles, but I also have a video on [Euclid's algorithm](https://youtu.be/ZcJMj0antos). 

- L2.1: *In which we learn how to execute functional programs on a **Stack Machine** and how to use recursion as a  problem solving technique.* 
    - What you need to be able to do at this stage: Run Haskell on your machine; being able to use Chapters 1-6 of [learnyouahaskell](http://learnyouahaskell.com/chapters) as a reference for writing your own small Haskell programs. 
    - The lecture notes of today are
        - [Rewriting and the Call Stack](https://hackmd.io/@alexhkurz/HJiulVg0U)
        - [Recursion as a Problem Solving Technique](https://hackmd.io/@alexhkurz/Sy7M_6yMF)
        - [Towers of Hanoi](https://hackmd.io/@alexhkurz/rJQwvpyMY)
    - Homework until Thursday:
        - Watch the first half of the video on [Dynamic Programming](https://www.youtube.com/watch?v=oBt53YbR9Kk). It has many examples of recursion as a problem solving technique. Moreover it teaches dynamic programming (= recursion + memoization) which is a popular topic at coding interviews. I also recommend doing the other examples (`gridTraveller`, `canSum`, `howSum`, `bestSum`, `canConstruct`, `countConstruct`, `allConstruct`). You can also try your own problems such as allSums (all sums). Make sure to think about your own answer before watching the solution in the video. The takeaway will be that once you have a correct solution, making it efficient using memoization will be easy. The second half of the video is about turning recursive solutions into iterative ones using tabulation.
    - Homework for the Report: See [Towers of Hanoi](https://hackmd.io/@alexhkurz/rJQwvpyMY).

## Parsing and Algebraic Data Types (An Interpreter for Arithmetic)

Arithmetic can be seen as a programming language (not Turing complete) and a calculator as an interpreter. This is where we are going to start. Importantly, the methods we learn will scale to proper real world programming languages.

- L 2.2: *In which we learn how to write our first interpreter.* 
  - The lecture notes of today are about implementing a calculator: 
    - [Overview](https://hackmd.io/@alexhkurz/HkpdXJ1fK) 
    - [Interpreter](https://hackmd.io/@alexhkurz/rJX-i1kzY)
    - [Virtual Machine](https://hackmd.io/@alexhkurz/H12igXkzK) 
  - Homework until Tuesday (not assessed but essential for understanding the material): 
    - See the sections marked as homework in the three notes above [Overview](https://hackmd.io/@alexhkurz/HkpdXJ1fK), [Interpreter](https://hackmd.io/@alexhkurz/rJX-i1kzY), [Virtual Machine](https://hackmd.io/@alexhkurz/H12igXkzK).
  - [Assignment 1](Assignment1/assignment1.md) will extend the [Virtual Machine](https://hackmd.io/@alexhkurz/H12igXkzK) from the lecture. I made some videos for reviewing some of the relevant programming techniques:
      - [Haskell Tips I](https://youtu.be/wj0j2HjMw6w)
      - [Haskell Tips II](https://youtu.be/naNLE4GLrTo)
      - [Recursion over algebraic data types in Haskell](https://www.youtube.com/watch?v=2YLfJvOtLwA)

- L 3.1: *In which we learn how to use a context-free grammar to generate a parser and build a calculator that processes concrete syntax.* 
    - Q&A and [feedback for HW1](https://hackmd.io/@alexhkurz/HkjeIJ602). Discussion of [projects](projects.md).
    - Lecture notes:
        - A [Short Introduction to Parsing](https://hackmd.io/@alexhkurz/BkSgRX1GF) (context free grammars, concrete syntax trees).
        - [A Calculator: Parser](https://hackmd.io/@alexhkurz/BkqOWbgMF) (abstract syntax trees, bnfc).    
    - [Code of the calculator](src/Calculator/)
    - Videos: [Order of Operations in CFGs](https://youtu.be/jf1xhZSpCvg) and [Uniqueness of Parse Trees](https://youtu.be/3ZLkPwB_c9g).
    - The lecture notes on [Abstract and Concrete Syntax](https://www.cse.chalmers.se/edu/year/2011/course/TIN321/lectures/proglang-02.html) from Aarne Ranta's Programming Languages Course.

## Lambda Calculus

The lambda calculus plays a foundational role in Programming Languages because it is the smallest programming language that has variables and functions (and, as an aside, is Turing complete).

- L 3.2: *AI programming and Lambda-Calculus*
    - [Programming with Aider](https://codeberg.org/alexhkurz/blockly-experiments/src/branch/main/README.md).
    - In the lecture, I gave a general introduction to lambda caluclus. 
    - Video: [Syntax of Lambda Calculus](https://youtu.be/D0kH1BpNr14).

- L 4.1: **In which we learn the syntax of lambda calculus.** 
    - [Feedback to Project Design](feedback-projects.md)
    - [BlocklyLambda](https://github.com/alexhkurz/BlocklyLambdaCalculus) is a block based lambda calculus with a compiler to JavaScript. Let me know if you test it or make further developments. 
    - Lecture: [Syntax of Lambda Calculus](https://hackmd.io/@alexhkurz/S1D0yP8Bw).
    - Videos: Parsing lambda-calculus expressions:  [one](https://youtu.be/eYstx7uuE6c) and [two](https://youtu.be/yls1NEUlzZA).

- L 4.2: *In which we learn the meaning of lambda calculus programs.* 
    - Lecture: [Semantics of Lambda Calculus](https://hackmd.io/@alexhkurz/H1e4Nv8Bv).  
    - Videos: [Operational Semantics of Lambda Calculus](https://www.youtube.com/watch?v=h4aT42t7v9c#t=0m) and [Reducing Lambda Expressions](https://youtu.be/for3Meg1Lbc)

- L 5.1: *In which we practice the execution of lambda calculus pen-and-paper*.
    - [Review](https://hackmd.io/@alexhkurz/SJBxebYA3)
    - Code: [A Lambda Calculus Interpreter](https://codeberg.org/alexhkurz/LambdaCalculus)
    - Lecture: [Running the Interpreter Pen-and-Paper](https://hackmd.io/@alexhkurz/r1HFipylT)

- L 5.2: 
    - [Review](https://hackmd.io/@alexhkurz/r1nJUL7xp): Concrete and abstract syntax.
    - [Review](https://hackmd.io/@alexhkurz/HkA20g7xT): Free and bound variables, capture avoiding substitution.
    - [Variables, Binding, Scope and Substitution](https://hackmd.io/@alexhkurz/SkQzDC6n7): Binders are everywhere.
    - [Church Encodings](https://hackmd.io/@alexhkurz/r1kDshkaX): Lambda calculus is Turing complete.

## Rewriting Theory, Invariants

- L 6.1: 
    - [Feedback on Projects](feedback-projects.md), Week 3 and 4.
    - Lecture: [Rewriting: Examples](https://hackmd.io/@alexhkurz/rkzITG4nD). We spent most of the time on the MU-puzzle. In one section we found a complete solution using arithmetic modulo three.
    
- L 6.2:
    - [String rewriting exercises](https://hackmd.io/@alexhkurz/Syn23oMHF). In the lecture we worked through the first couple of examples.
    - [Rewriting: Introduction](https://hackmd.io/@alexhkurz/BJ7AoGcVK). Contains the homework and a short version of the important definitions.

- L 7.1: [Confluence and Normal Forms](https://hackmd.io/@alexhkurz/B1wB3rT4F). 

- L 7.2: *In which we learn about invariants as a problem solving technique.*
    - Puzzles:
        - Chessboard puzzle
        - [The Tetris Puzzle](https://hackmd.io/@alexhkurz/H18MAZr-T)
        - [SlidingPuzzle](https://alexhkurz.github.io/SlidingPuzzle/)
    - [Invariants](https://hackmd.io/@alexhkurz/r1dp-LBBt).

- Week 8: Midterm Project Presentations
## Operational and Denotational Semantics

- L 9.1: [Operational and Denotational Semantics](https://hackmd.io/@alexhkurz/Hkf6BTL6P).
- L 9.2: [Equivalence Relations](https://hackmd.io/@alexhkurz/SJD8iWIfp).
- L 10.1: Code Review: We talked about how to do a code review using [BlocklyPrisonersDilemma](https://github.com/alexhkurz/BlocklyPrisonersDilemma) as an example: Issues, Pull Requests, Debuggers.
- L 10.2: I started with some comments on Greg Meredith's [Agency and Control](resources/Meredith-Agency%20and%20Control.pdf). For the rest of the lecture I discussed how one can use normal forms to compute with infinite sets.


- L 11.1: [Normalisation by Evaluation](https://hackmd.io/w9RLzXmcS86U4HVAQi5Lqg).
- L 11.2: [Completness of Propositional Logic](https://hackmd.io/@alexhkurz/HJf4IknmT).

## An Interpreter for a Functional Programming Language

- L 12.1: Extending Lambda Calculus:
    - Code: [LambdaNat](https://codeberg.org/alexhkurz/LambdaNat/src/branch/main/README.md)
    - Exercise: [LambdaNat2](https://hackmd.io/@alexhkurz/r1i33_-N6)
- L 12.2:
    - [Extensions of lambda-calculus](https://hackmd.io/@alexhkurz/rJEeYqZtw), video review of the [fixed point combinator](https://youtu.be/XvDOwbSh3xE)

- [Assignment 2](assignment2.md)

- L 13.1 More on Assignment 2.

## Type Theory

- L 13.2: [Typed Lambda Calculus](https://hackmd.io/@alexhkurz/S1Sopqo6w).

## Mathematics as a Programming Langauge

- L14.1: 
    - In Section 1 and 3,  we discussed how to use invariants to implement a provably correct solution of the [Prisoners and Hat Puzzle](https://www.youtube.com/watch?v=N5vJSNXPEwA).
    - In Section 2, I gave a lecture on Mathematics as a Programming Languages based on the *Additional Material* below. I also have some older notes on the topic, [here](https://hackmd.io/@alexhkurz/SyfHx1v_Q) and [here](https://hackmd.io/@alexhkurz/ByGLTvFDE).

---

## Additional Material

### More Theory

The following are notes from previous years that did not make into this year (some links may be broken, I will clean this up later).

- Logic:
    - [Induction and Equational Reasoning](https://hackmd.io/02w2FuLsT_uKYQxkPSdvtw), [Induction](https://hackmd.io/@alexhkurz/HJG08mJvY)
    - Proving termination:
        - [Halting Problem](https://hackmd.io/@alexhkurz/HyHUQYXuY)
        - [Termination](https://hackmd.io/@alexhkurz/BJoZF44Iw)
        - [Finitely Branching ARSs](https://hackmd.io/@alexhkurz/rkX-t-HdH)
        - [Rules of logic](https://hackmd.io/xJ8NOiK4S5qnYvEI85bHig), https://hackmd.io/@alexhkurz/Hyxy7veIKX
    - Verifying  safety properties:
        - [Hoare Logic](https://hackmd.io/Df57tnuCSGaW8wqqsl57FQ) ... [Exercises](https://hackmd.io/@alexhkurz/rkhVZNzjH)
        - [Dafny](https://hackmd.io/@alexhkurz/SJyBbDQjv)
    - [Theorem Proving](https://hackmd.io/JrBBURefROGD1xMN44Zivw), [Theorem Proving](https://hackmd.io/@alexhkurz/HJnQDm1wK)
- Universal Algebra:    
    - [Universal Algebra 1](https://hackmd.io/@alexhkurz/Hk7_Dfyj7)
    - [Universal Algebra 2](https://hackmd.io/@alexhkurz/HyqLPrWsH), https://hackmd.io/@alexhkurz/Bymo_vCj7
    - [Universal Algebra 3](https://hackmd.io/@alexhkurz/SyscwH-iS), https://hackmd.io/@alexhkurz/By3OtPAsQ
    - [Universal Algebra 4](https://hackmd.io/@alexhkurz/r1_avBbor), https://hackmd.io/@alexhkurz/HyMesfK3Q
- Category Theory
    - [Introduction](https://hackmd.io/@alexhkurz/ryhYFFtmD)
    - [Category Theory - An Axiomatic Theory of Structure](https://hackmd.io/@alexhkurz/H1OxumxRP)

### A Puzzle

- [Prisoners and Hat Puzzle](https://www.youtube.com/watch?v=N5vJSNXPEwA)










