# Teaching Rationale

In my courses I like to set students and myself ambitious aims. These can be achieved, but only if students show initiative and take responsibility for their own learning. 

For example, in Programming Languages, the aim is that students learn how to implement their own programming language. In Algorithm Analysis, the aim is to learn how to bridge the gap between the theory and practice of algorithms. 

In both cases, this means that the course is divided into theory and practice. Theory is presented in the lectures. Practice involves assignments and projects.

Learning theoretical computer science requires to develop mathematical skills. This is why these courses have discrete mathematics as prerequisites. The very term *algorithm analysis* refers to the *mathematical* analysis of algorithms which involves both time and space complexity as well as proving that an implementation meets its specification. Moreover, much of theoretical computer science can ultimately be traced back to the aim of constructing efficient compilers for increasingly sophisticated (general purpose or domains specific) *programming languages*. 

Based on these considerations, my courses are designed as follows:

**Lectures and Homework:**

- In a typical week, two lectures presents one topic from theoretical computer science.
- Students have one week to work on homework practicing some of the relevant skills. At this stage I don't expect students to master the material. I grade this homework for completion. 
- I will give general feedback in class on the homework. From this point on, I expect students to be responsible for the correctness of their answers. If doubts remain, students should ask me after class or in office hours or via email.
- I am aware that a new topic every week is challenging, but it is the only way to get you to the next level. I hope that at the end of the semester, you are beginning to see how everything hangs together. 
- For the final report, the weekly homework should be embedded in a narrative that shows the student's understanding of how the theoretical topics fit into the larger area of software engineering.

<!-->
**Project:**

- ... 

## remarks specific to algorithm analysis

(... under construction ... based on lessons learned from 2023 in preparation for 2024 ...)

- Cover a broad range of algorithms (at least one per week). 
- Select algorithms that illustrate fundamental concepts that come up in a wide variety of different applications.

What I dont cover:

- We only touch on tree or graph traversal and search algorithms. These algorithms are studied in depth in other courses.
- Recursive programming. This is at the core of my course on Programming Languages.
- Dynamic programming. Dynamic programming is recursive programming plus memoization. We do some of that in Programming Languages.

...
-->