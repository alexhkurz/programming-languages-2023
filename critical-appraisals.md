# Critical Appraisals

I will occasionally ask you to accompany technical work with what I call a critical appraisal. 

A critical appraisal is an opportunity to step back and reflect on what went well, or not, and why, as well as to highlight what you learned from your work. 

An insightfull critical appraisal helps me
- to understand what you learnd from the assignment and
- to improve my assignments in the future.

(At this point, the AI suggests "A critical appraisal is not a place to complain about the assignment or to ask for a better grade" and while that may be true, it is also not the main point I want to make.)

For example, in a programming assignment, the critical appraisal should contain:
- a list of requirements you didn't implement (if applicable),
- a discussion of where you run into difficulties,
- how you tested your programs,
- what you learned, 
- interesting observations you made, 
- how you connect the programming assignment with the theory taught in the lecture,
- a list of interesting questions that remain,
- whatever else you consider relevant.

To summarize, after spending a considerable amount of time on an assignment, I do expect you to have something interesting to say that reflects your own experience and unique personal insights stemming from your struggles with the subject.

**Tip:** If you take notes for the critical appraisal during the work on the assignment it should be very little extra work.

**Layout:** Make use of markdown features such as *italic*, **bold**, [links](https://www.markdownguide.org/cheat-sheet/), etc.

