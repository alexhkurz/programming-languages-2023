(\f.\x.f(f x)) (\f.\x.(f(f(f x))))

\x.(\f.\x.(f(f(f x)))) (\f.\x.(f(f(f x)))x)

\x.(\f.\x.(f(f(f x)))) (\x.(f(f(f x)))x)

both now have "\x.(f(f(f x)))" in the equation...
continue with applications of lambda:

\x.(\x.(f(f(f x))))(f(f(f x)))
simplifies to:
\x.f(f(f(f(f(f x)))))