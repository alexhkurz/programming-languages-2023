(\f.\x.f(f x))(\f.\x.f(f x)) =
(\x.(\f.\x.f(f x))((\f.\x.f(f x)) x)) =
(\x.(\f.\x.f(f x))((\f.\x0.f(f x0)) x)) =
(\x.(\f.\x.f(f x))(\x0.x(x x0))) =
(\x.(\f.\x1.f(f x1))(\x0.x(x x0))) =
(\x.(\x1.(\x0.x(x x0))((\x0.x(x x0)) x1))) =
(\x.(\x1.(\x0.x(x x0))(x(x x1)))) =
\x.(\x1.x(x(x(x x1)))) 


(\f.\x.f(f x))(\f.\x.f x) =
(\x.(\f.\x.f x)((\f.\x.f x) x)) =
(\x.(\f.\x.f x)((\f.\x0.f x0) x)) =
(\x.(\f.\x.f x)(\x0.x x0)) =
(\x.(\f.\x1.f x1)(\x0.x x0)) =
(\x.(\x1.(\x0.x x0) x1)) =
\x.(\x1.x x1)


(\f.\x.f x)(\f.\x.f(f x)) =
\x0.(\f.\x.f(f x)) x0 =
\x0.\x1.x0(x0 x1) 


(\f.\x.f x)(\f.\x.f x) =
\x0.(\f.\x.f x) x0 =
\x0.\x1.x0 x1


eval :: Exp -> Exp  
eval (App e1 e2) = case eval e1 of
    (Abs i e3) -> eval (subst i (eval e2) e3)
    e3 -> App e3 (eval e2)
eval (Abs i e) = Abs i (eval e)
eval x = x 


-- (\x.\y.x) y
eval (App (Abs (Id "x") (Abs (Id "y") (Var (Id "x")))) (Var (Id "y"))) = 
eval (subst (Id "x") (Var (Id "y")) (Abs (Id "y") (Var (Id "x")))) =
eval (subst (Id "x") (Var (Id "y")) (Abs (Id "y") (Var (Id "x")))) =
...